/**
 * 
 */
package com.invest.cardpay.proxy;

/**
 * @author Tabakov
 *
 */
public class CardPayInfo {

    private 	String note;
    private String orderXMLEncoded;
    private String orderXML;
    private String sha512;
	
	public CardPayInfo(String orderXmlEncoded) {
		this.orderXMLEncoded = orderXmlEncoded;
	}

	public String getOrderXML() {
		return orderXML;
	}

	public void setOrderXML(String orderXML) {
		this.orderXML = orderXML;
	}

	public String getOrderXMLEncoded() {
		return orderXMLEncoded;
	}

	public void setOrderXMLEncoded(String orderXMLEncoded) {
		this.orderXMLEncoded = orderXMLEncoded;
	}


	public String getNote() {
		return note;
	}


	public void setNote(String note) {
		this.note = note;
	}

	public String getSha512() {
		return sha512;
	}

	public void setSha512(String sha512) {
		this.sha512 = sha512;
	}

	@Override
	public String toString() {
		return "CardPayInfo [note=" + note + ", orderXMLEncoded=" + orderXMLEncoded + ", orderXML=" + orderXML
				+ ", sha512=" + sha512 + "]";
	}

}
